#!/bin/bash
#
# Start monitoring the DPC at a later time. Accepts all time specifications
# allowed by the at(1) command.
#

if [ $# -lt 1 ]
then
    echo "Usage: $(basename $0) timespec" 1>&2
    exit 1
fi

echo "redis-cli rpush mon:commands start > /dev/null" | at "$@"
