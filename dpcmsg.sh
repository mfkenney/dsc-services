#!/bin/bash
#
# Queue a message for the DPC.
#

: ${DSCHOST=localhost}
MSGQUEUE="dpc:commands"
msgid="$(uuid)"

redis-cli -h $DSCHOST rpush $MSGQUEUE "{\"id\": \"$msgid\", \"body\": \"$*\"}"
