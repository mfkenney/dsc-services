#!/bin/bash
#
# Read and display A/D values on the DSC or DPC
#
PATH=$PATH:/usr/local/bin
export PATH

if [ -e /initrd/ts4200.subr ]
then
    # TS-4200 (DPC controller)
    ADC_BASE=0x30000080
    BOARD="TS-4200"
else
    ADC_BASE=0xb0016000
    BOARD="TS-4800"
fi
ADC_CFG=$((ADC_BASE + 0))
ADC_CHANSEL=$((ADC_BASE + 2))

if [ "$(whoami)" != "root" ]
then
    PROG="sudo peekpoke"
else
    PROG="peekpoke"
fi

# Convert an ADC channel number (1-based) to
# an FPGA register address
ADC_CHAN () {
    echo $(($1*2 + 2 + ADC_BASE))
}

adc_init () {
    # Initialize the ADC
    #
    # 1. Configure AN_SEL for TS-81X0 base board
    # 2. Set sampling speed to 15hz (slowest speed)
    # 3. Set gain to 0
    # 4. Enable the single-ended channels (3-6)
    $PROG 16 $ADC_CFG 0xad18
    $PROG 16 $ADC_CHANSEL 0x003c
}

adc_read () {
    # Read all of the single-ended channels
    $PROG 16 $(ADC_CHAN 3)
    $PROG 16 $(ADC_CHAN 4)
    $PROG 16 $(ADC_CHAN 5)
    $PROG 16 $(ADC_CHAN 6)
}

form_init () {
    title="$1"

    n=${#title}
    col=$(($(tput cols)/2 - $n/2))
    tput cup 0 $col
    echo -n $title
    row=4
    col=20
    tput bold
    set -- Ain3 Ain4 Ain5 Ain6
    for label
    do
        tput cup $row $col
        echo -n $label
        row=$((row + 1))
    done
    tput cup 0 0
    echo -n "^c to exit"
    tput sgr0
}

form_update () {
    row=4
    col=54
    for value
    do
        tput cup $row $col
        tput el
        # Convert hex to decimal
        x=$(printf "%5d" $value)
        # Convert A/D counts to volts
        v=$(echo "scale=3;$x*10.24/65536"|bc)
        printf "%-6.3f V" $v
        row=$((row + 1))
    done
}

do_sample () {
    tstamp=$(sleepenh 0)
    while true
    do
        form_update $(adc_read)
        tstamp=$(sleepenh $tstamp 2)
    done
}

display_init () {
    tput civis
    tput clear
    tput sc
}

display_restore () {
    tput cnorm
    tput clear
    tput rc
}

adc_init
display_init
trap "display_restore;exit 0" 0 1 2 3 15
form_init "$BOARD A/D Input"
do_sample
