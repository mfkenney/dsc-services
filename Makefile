#
# Makefile to install service scripts.
#
BINDIR := $(HOME)/bin
SVCDIR := $(HOME)/sv
SYS_SVCDIR := /etc/sv

SVCS := archiver zmqapi prstatus chkwifi
SYS_SVCS := runsvdir-rsn
PROGS := venv_helper.sh 

.PHONY: install install-sv install-sys install-bin

all: install-sv install-bin

install-sv: $(SVCS)
	cp -a -v -t $(SVCDIR) $^

install-sys: $(SYS_SVCS)
	sudo cp -a -v -t $(SYS_SVCDIR) $^

install-bin: $(PROGS)
	install -d $(BINDIR)
	install -m 755 -t $(BINDIR) $^
